extension StringHelper on String {
  ///Returns the [String] that repeats this [String] [quantity] times:
  ///
  ///Example:
  ///```dart
  ///'Hello'.repeat(3) == 'HelloHelloHello'
  ///```
  String repeat(int quantity) => List<String>.filled(quantity, this)
      .reduce((value, element) => value + element);

  ///Replaces the first letter in [this] [String] with
  ///upper case one:
  ///
  ///Example:
  ///```dart
  ///'hello World'.toUpperCaseFirst() == 'Hello World'
  ///
  ///'helloWorld'.toUpperCaseFirst() == 'HelloWorld'
  ///
  ///'HelloWorld'.toUpperCaseFirst() == 'HelloWorld'
  ///
  ///'123!@helloWorld'.toUpperCaseFirst() == '123!@HelloWorld'
  ///```
  String toUpperCaseFirst() {
    final index = indexOf(RegExp(r'[A-Za-zА-Яа-я]'));
    if (index >= 0) {
      return replaceRange(index, index + 1, this[index].toUpperCase());
    }
    return this;
  }

  ///Replaces the first letter in this [String] with
  ///lower case one:
  ///
  ///Example:
  ///```dart
  ///'Hello World'.toLowerCaseFirst() == 'hello World'
  ///
  ///'HelloWorld'.toLowerCaseFirst() == 'helloWorld'
  ///
  ///'helloWorld'.toLowerCaseFirst() == 'helloWorld'
  ///
  ///'123!@HelloWorld'.toLowerCaseFirst() == '123!@helloWorld'
  ///```
  String toLowerCaseFirst() {
    final index = indexOf(RegExp(r'[A-Za-zА-Яа-я]'));
    if (index >= 0) {
      return replaceRange(index, index + 1, this[index].toLowerCase());
    }
    return this;
  }

  ///Returns [this] if [condition] is true.
  ///
  ///Else returns empty [String]
  String thisOrEmpty(bool condition) => condition ? this : '';
}
