extension MapHelper<K, V> on Map<K, V> {
  ///Returns true if [Map] contains ALL keys from [keys]
  ///
  ///Else returns false
  bool containsAllKeys(Iterable<K> keys) => keys.fold(true,
      (previousValue, element) => containsKey(element) ? previousValue : false);

  ///Same as Iterable's [fold], but for [Map]
  ///
  ///Reduces [Map] to a single element
  ///
  ///Takes a [function] and repeatedly applys it
  ///to all [Map] members while putting the results
  ///into accumulator with initial value = [initialValue],
  ///as shown bellow:
  ///```dart
  ///var acc = initialValue;
  ///forEach((key, value) {
  ///acc = function(acc, key, value);
  ///});
  ///return acc;
  /// ```
  O fold<O>(
      O initialValue, O Function(O previousValue, K key, V value) function) {
    var acc = initialValue;
    forEach((key, value) {
      acc = function(acc, key, value);
    });
    return acc;
  }

  ///Same as Iterable's [every], but for [Map]
  ///
  ///Tests every member of this [Map] with
  ///[testFunc]. Returns true if all members of this [Map]
  ///will pass the [testFunc]. Else returns false.
  ///
  ///If [Map] is empty - returns true
  ///```dart
  ///var result = true;
  ///forEach((key, value) {
  ///  result = result && testFunc(key, value);
  ///});
  ///return result;
  ///```
  bool every(bool Function(K key, V value) testFunc) {
    var result = true;
    forEach((key, value) {
      result = result && testFunc(key, value);
    });
    return result;
  }
}
