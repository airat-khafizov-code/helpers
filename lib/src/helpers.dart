import 'package:helpers/src/exceptions.dart';

class Helper {
  ///Returns String name of the enum variable
  ///
  ///[enumMember] should be an exemplar of Dart enum
  static String getEnumName(Object enumMember) =>
      enumMember.toString().split('.').last;

  ///Throws [GenericException] with rule number if rules are broken
  ///
  ///If all rules are true - returns [RuleCheckResult.allTrue]
  static RuleCheckResult ruleCheck(List<bool> rules, {String? rulerName}) {
    for (var i = 0; i < rules.length; i++) {
      if (!rules[i]) {
        throw GenericException(
            message:
                'Rule #$i from ${rulerName ?? 'unnamed'} ruleCheck is broken!',
            source: rulerName);
      }
    }
    return RuleCheckResult.allTrue;
  }

  ///Itarates over the [List<Cont2>]
  ///
  ///Returns [obj2] from [Cont2] if the rule [obj1] in this [Cont2] is broken
  ///
  ///If [throwInsteadOfReturn] is true -  then [obj2] will be thrown
  ///
  ///If all rules are true - returns [RuleCheckResult.allTrue]
  ///
  ///Note: [obj2] can't be a [RuleCheckResult.allTrue]
  static Object? returningRuleCheck(List<Cont2<bool, Object?>> rules,
      {bool throwInsteadOfReturn = false}) {
    for (var i = 0; i < rules.length; i++) {
      if (!rules[i].obj1) {
        if (rules[i].obj2 == RuleCheckResult.allTrue) {
          throw GenericException(
              message: 'Rule #&i - obj2 can\'t be RuleCheckResult.allTrue');
        }
        if (throwInsteadOfReturn && rules[i].obj2 != null) {
          throw rules[i].obj2!;
        }
        return rules[i].obj2;
      }
    }
    return RuleCheckResult.allTrue;
  }
}

///Container for two objects with types [T1] and [T2]
class Cont2<T1, T2> {
  ///First object
  T1 obj1;

  ///Second object
  T2 obj2;

  ///Container for two objects with types [T1] and [T2]
  Cont2(this.obj1, this.obj2);
}

///Rule check results
enum RuleCheckResult { allTrue }
