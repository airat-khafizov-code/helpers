extension IterableHelper<E> on Iterable<E> {
  ///Same as [firstWhere], but instead of errors
  ///returns [null]
  E? firstWhereOrNull(bool Function(E element) test) {
    for (var el in this) {
      if (test(el)) {
        return el;
      }
    }
    return null;
  }

  ///Same as [lastWhere], but instead of errors
  ///returns [null]
  E? lastWhereOrNull(bool Function(E element) test) {
    late E result;
    var foundMatching = false;
    for (var el in this) {
      if (test(el)) {
        result = el;
        foundMatching = true;
      }
    }
    if (foundMatching) {
      return result;
    }
    return null;
  }

  ///Same as [singleWhere], but instead of errors
  ///returns [null]
  E? singleWhereOrNull(bool Function(E element) test) {
    late E result;
    var foundMatching = false;
    for (var element in this) {
      if (test(element)) {
        if (foundMatching) {
          return null;
        }
        result = element;
        foundMatching = true;
      }
    }
    if (foundMatching) {
      return result;
    }
    return null;
  }
}
