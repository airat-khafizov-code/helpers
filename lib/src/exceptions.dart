///[GenericException] is [Exception] with
///overriden [toString] method and more params
///in constructor
class GenericException implements Exception {
  final String _name = 'Exception';

  ///Message in human-readable form
  final String message;

  ///Source of this [Exception]
  final dynamic source;

  ///[GenericException] is [Exception] with
  ///overriden [toString] method and more params
  ///in constructor
  const GenericException({this.message = '', this.source});

  @override
  String toString() => '$_name${message != '' ? ': $message' : '!'}\n'
      '${source != null ? 'Source: ${source.toString()}\n' : ''}';
}

///[GenericException] for type check errors
class TypeCheckException extends GenericException {
  @override
  final String _name = 'TypeCheckException';

  ///[GenericException] for type check errors
  const TypeCheckException({String message = '', dynamic source})
      : super(message: message, source: source);
}
